﻿namespace DigitalExam.AppCode
{
    public class Loesung
    {
        float? note;
        string? text;

        /// <summary>
        /// 
        /// </summary>
        public float? Note { get { return note; } private set { note = value; } } 


        public Loesung()
        {
            this.text = null;
            note = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        public Loesung(string text) : this()
        {
            this.text = text;
        }
    }
}
