﻿namespace DigitalExam.AppCode
{
    public class DigitaleKlausur
    {
        string titel;
        string text;
        DateTime date;

        ICollection<Student> angemeldeteStudenten;
        IDictionary<Student, Loesung> abgegebeneLoesungen;

        private DigitaleKlausur()
        {
            this.angemeldeteStudenten = new List<Student>();
            this.abgegebeneLoesungen = new Dictionary<Student, Loesung>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="titel"></param>
        /// <param name="text"></param>
        /// <param name="date"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public DigitaleKlausur(string titel, string text, DateTime date) : this()
        {
            this.titel = titel ?? throw new ArgumentNullException(nameof(titel));
            this.text = text ?? throw new ArgumentNullException(nameof(text));
            this.date = date;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="student"></param>
        /// <exception cref="InvalidOperationException">Wenn Anmeldung unter einer Woche vor beginn</exception>
        public void Anmelden(Student student)
        {
            var today = DateTime.Now;
            var delta = today - date;

            if (delta.Days <= 7) throw new InvalidOperationException("Klausuranmeldung nicht länger möglich");


            this.angemeldeteStudenten.Add(student);
        }

        public void Abgabe(Student student, string? loesung)
        {
            Loesung loesung1 = new Loesung(loesung);
        }
    }
}
