﻿namespace DigitalExam.AppCode
{
    public class Student
    {
        string name;
        int matrikelNummer;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="matrikelNummer"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public Student(string name, int matrikelNummer)
        {
            this.name = name ?? throw new ArgumentNullException(nameof(name));
            this.matrikelNummer = matrikelNummer;
        }
    }
}
