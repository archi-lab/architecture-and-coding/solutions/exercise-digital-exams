# Übung "Digitale Klausuren"

In dieser Übung geht es darum, aus einem einfachen Anforderungstext eine Klassenstruktur zu definieren und
diese in Code zu überführen. Mit Hilfe eines einfachen Zustandsmodells kann Business-Logik visualisiert und 
dann implementiert werden. 

Alle Details gibt es hier: 
[https://www.archi-lab.io/exercises/csharp/digitalExam.html](https://www.archi-lab.io/exercises/csharp/digitalExam.html)